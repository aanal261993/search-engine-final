package com.alfa.sorting;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Sorting {

	public static Map<String, Integer> getFrequency(String words, List<String> resultDocs,
			Map<String, Map<String, Integer>> wordToDocumentMap) {
		String processedQuery = words.trim().toLowerCase();
		String[] wordArray = processedQuery.split("\\s+");

		Map<String, Integer> fileRank = new HashMap<String, Integer>();

		for (String word : wordArray) {
			Map<String, Integer> documentToWordCount = wordToDocumentMap.get(word);
			for (String document : resultDocs) {
				Integer wordCount = documentToWordCount.get(document);
				// System.out.println("Word " + word + " found " + wordCount + " times in
				// document " + document);
				if (fileRank.containsKey(document)) {
					Integer currentCount = fileRank.get(document);
					fileRank.put(document, wordCount + currentCount);
				} else {
					fileRank.put(document, wordCount);
				}

			}
		}
		Map<String, Integer> sortedFileRank = sortMapByValues(fileRank);

		return sortedFileRank;
	}

	public static Map<String, Integer> sortMapByValues(Map<String, Integer> fileRank) {
		Map<String, Integer> sortedFileRank = fileRank.entrySet().stream()
				.sorted(Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		return sortedFileRank;
	}
	
	public static Map<String, Double> sortMapByValuesDouble(Map<String, Double> fileRank) {
		Map<String, Double> sortedFileRank = fileRank.entrySet().stream()
				.sorted(Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		return sortedFileRank;
	}
}
